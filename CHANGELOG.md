# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.3.7 - 2024-12-09(13:52:55 +0000)

## Release v0.3.6 - 2024-10-02(18:45:08 +0000)

## Release v0.3.5 - 2024-08-27(14:25:23 +0000)

### Other

- regression leaves firewall open after reboot

## Release v0.3.4 - 2024-02-08(08:31:43 +0000)

### Other

- wireguard: enable =1 -> enable=0 -> enable=1 not working

## Release v0.3.3 - 2023-11-30(14:03:33 +0000)

### Other

- mod-fw-amx: extend to support get_services(filter) function (or similar)

## Release v0.3.2 - 2023-06-27(07:48:03 +0000)

### Changes

- - [tr181-firewall] it is not possible to use TCP and UDP keyword to open firewall

## Release v0.3.1 - 2023-01-12(14:45:40 +0000)

### Changes

- random: dropbear is not running

## Release v0.3.0 - 2022-11-15(12:48:58 +0000)

### New

- add firewall rules to forward traffic for PCP mapped packets

## Release v0.2.1 - 2022-10-26(08:15:54 +0000)

### Fixes

- remove amxm destructor to prevent invalid read

## Release v0.2.0 - 2022-04-29(13:28:38 +0000)

### Other

- [amx][SSH Server plugin] Integrate amx SSH server on SOP

## Release v0.1.0 - 2022-03-01(09:25:45 +0000)

### New

- It must be possible to open the firewall for the relay service

