/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "utils.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_path.h>
#include <amxb/amxb.h>

int get_entries(const char* dm_id, const char* entrytypename, amxc_var_t* args, amxc_var_t* ret) {
    int rv = -1;

    amxc_var_t fw_ret;
    amxc_string_t query_str;
    when_failed_trace(amxc_var_init(&fw_ret), end, ERROR, "amxc_var_init failed");
    when_failed_trace(amxc_string_init(&query_str, 32), cleanup0, ERROR, "amxc_string_init failed");
    when_failed_trace(amxc_var_set_type(ret, AMXC_VAR_ID_LIST), cleanup, ERROR, "amxc_var_set_type failed");

    amxb_bus_ctx_t* bus = amxb_be_who_has("Firewall.");
    when_null_trace(bus, cleanup, ERROR, "No bus for 'Firewall.'");

    const char* filter = GET_CHAR(args, "filter-on-id");
    if(filter) {
        amxc_string_setf(&query_str, "Firewall.%s.[%s matches %s].", entrytypename, dm_id, filter);
    } else {
        amxc_string_setf(&query_str, "Firewall.%s.*", entrytypename);
    }

    rv = amxb_get(bus, amxc_string_get(&query_str, 0), 0, &fw_ret, 5);
    if((rv != AMXB_STATUS_OK) && (rv != amxd_status_object_not_found)) {
        SAH_TRACEZ_ERROR(ME, "amxb_get() failed for %s: %d", amxc_string_get(&query_str, 0), rv);
        goto cleanup;
    }
    if(rv == amxd_status_object_not_found) {
        SAH_TRACEZ_ERROR(ME, "querying returned not found");
        when_failed_trace(amxc_var_set_type(&fw_ret, AMXC_VAR_ID_LIST), cleanup, ERROR, "amxc_var_set_type failed");
    }

    //fw_ret type is variant_list<variant_htable<>>
    amxc_var_for_each(htable, &fw_ret) {
        amxc_var_for_each(htable_elem, htable) {
            SAH_TRACEZ_NOTICE(ME, "adding result %s", GETP_CHAR(htable_elem, dm_id));
            amxc_var_t* e = amxc_var_add(cstring_t, ret, GETP_CHAR(htable_elem, dm_id));
            when_null_trace(e, cleanup, ERROR, "amxc_var_add failed");
        }
    }

    rv = 0;

cleanup:
    amxc_string_clean(&query_str);
cleanup0:
    amxc_var_clean(&fw_ret);

end:

    return rv;
}

//@param fw_args must be inited to AMXC_VAR_ID_HTABLE
//@return 0 if ok
int translate_args_to_fwargs(const amxc_var_t* args, const translate_t* translate, amxc_var_t* fw_args) {
    int rv = -1;

    for(int i = 0; (translate[i].in_name != NULL) && (translate[i].out_name != NO_OUTNAME); i++) {
        amxc_var_t* fw_arg = NULL;
        const amxc_var_t* data = GET_ARG(args, translate[i].in_name);
        if(data == NULL) {
            continue;
        }
        fw_arg = amxc_var_add_new_key(fw_args, translate[i].out_name);
        if(translate[i].action != NULL) {
            when_failed(translate[i].action(data, fw_arg), end);
        } else {
            when_failed(amxc_var_convert(fw_arg, data, translate[i].type), end);
        }
    }

    rv = 0;
end:
    return rv;
}

