/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include "mod_fw_amx.h"
#include "utils.h"

#ifndef ID
#define ID "alias"
#define DM_ID "Alias"
#endif
#ifndef INTERFACE
#define INTERFACE "interface"
#endif
#ifndef ACTION
#define ACTION "_action"
#endif

int translate_iana_protocols_name(const amxc_var_t* data, amxc_var_t* fw_arg) {
    int rv = -1;

    amxc_var_t protocols;
    amxc_var_t new_list;

    when_failed(amxc_var_init(&protocols), exit);
    when_failed(amxc_var_init(&new_list), cleanup0);
    when_failed(amxc_var_set_type(&new_list, AMXC_VAR_ID_LIST), cleanup);
    when_failed(amxc_var_convert(&protocols, data, AMXC_VAR_ID_CSV_STRING), cleanup);
    when_failed(amxc_var_cast(&protocols, AMXC_VAR_ID_LIST), cleanup);
    amxc_var_for_each(protocol, &protocols) {
        struct protoent* proto = getprotobyname(GET_CHAR(protocol, NULL));
        if(proto) {
            amxc_var_add(uint32_t, &new_list, proto->p_proto);
        } else {
            amxc_var_add(cstring_t, &new_list, GET_CHAR(protocol, NULL));
        }
    }
    when_failed(amxc_var_convert(fw_arg, &new_list, AMXC_VAR_ID_CSV_STRING), cleanup);

    rv = 0;

cleanup:
    amxc_var_clean(&new_list);
cleanup0:
    amxc_var_clean(&protocols);

exit:
    return rv;
}

int set_service(UNUSED const char* function_name,
                amxc_var_t* args,
                amxc_var_t* ret) {
    amxc_var_t fw_args;
    amxc_var_t fw_ret;
    int rv = -1;
    const translate_t translate[] = {
        { "id", ID, AMXC_VAR_ID_CSTRING, NULL },
        { "interface", INTERFACE, AMXC_VAR_ID_CSTRING, NULL },
        { "ipversion", "ipversion", AMXC_VAR_ID_UINT32, NULL },
        { "icmp_type", "icmpType", AMXC_VAR_ID_UINT32, NULL },
        { "source_prefix", "sourcePrefix", AMXC_VAR_ID_CSTRING, NULL },
        { "destination_port", "destinationPort", AMXC_VAR_ID_CSTRING, NULL },
        { "protocol", "protocol", AMXC_VAR_ID_CSV_STRING, translate_iana_protocols_name },
        { "action", ACTION, AMXC_VAR_ID_CSTRING, NULL },
        { "enable", "enable", AMXC_VAR_ID_BOOL, NULL },
        { NULL, NULL, AMXC_VAR_ID_NULL, NULL }
    };

    when_failed(amxc_var_init(&fw_ret), exit);
    when_failed(amxc_var_init(&fw_args), cleanup0);
    when_failed(amxc_var_set_type(&fw_args, AMXC_VAR_ID_HTABLE), cleanup);
    when_failed(amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE), cleanup);

    when_null_trace(GET_ARG(args, "protocol"), cleanup, ERROR, "Missing protocol");

    when_failed(translate_args_to_fwargs(args, translate, &fw_args), cleanup);

    when_false(firewall_call("setService", &fw_args, &fw_ret), cleanup);

    amxc_var_add_key(cstring_t, ret, "id", GETP_CHAR(&fw_ret, "0.Alias"));

    rv = 0;
cleanup:
    amxc_var_clean(&fw_args);
cleanup0:
    amxc_var_clean(&fw_ret);
exit:
    return rv;
}

int get_services(UNUSED const char* function_name, amxc_var_t* args, amxc_var_t* ret) {
    return get_entries(DM_ID, "Service", args, ret);
}

int delete_service(UNUSED const char* function_name,
                   amxc_var_t* args,
                   amxc_var_t* ret) {
    amxc_var_t fw_args;
    amxc_var_t fw_ret;
    int rv = -1;

    when_failed(amxc_var_init(&fw_ret), exit);
    when_failed(amxc_var_init(&fw_args), cleanup0);
    when_failed(amxc_var_set_type(&fw_args, AMXC_VAR_ID_HTABLE), cleanup);
    when_failed(amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE), cleanup);

    when_str_empty_trace(GET_CHAR(args, "id"), cleanup, ERROR, "Missing id");
    amxc_var_add_key(cstring_t, &fw_args, ID, GET_CHAR(args, "id"));

    when_false(firewall_call("deleteService", &fw_args, &fw_ret), cleanup);

    rv = 0;
cleanup:
    amxc_var_clean(&fw_args);
cleanup0:
    amxc_var_clean(&fw_ret);
exit:
    return rv;
}
