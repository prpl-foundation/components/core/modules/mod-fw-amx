/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>
#include <amxb/amxb.h>

#include "mod_fw_amx.h"

bool firewall_call(const char* function, amxc_var_t* args, amxc_var_t* ret) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("Firewall.");
    int rv = -1;

    when_null_trace(bus, exit, ERROR, "No bus for 'Firewall.'");
    rv = amxb_call(bus, "Firewall.", function, args, ret, 5);
    when_false_trace(rv == AMXB_STATUS_OK, exit, ERROR, "amxb_call(%s, ...) failed: %d", function, rv);

exit:
    return rv == AMXB_STATUS_OK;
}

bool firewall_interface_call(const char* interface, const char* function, amxc_var_t* args, amxc_var_t* ret) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("Firewall.");
    amxc_string_t objectname;
    int rv = -1;

    when_null_trace(bus, exit, ERROR, "No bus for 'Firewall.'");
    when_failed_trace(amxc_string_init(&objectname, 64), exit, ERROR, "amxc_string_init failed");
    when_failed_trace(amxc_string_setf(&objectname, "Firewall.Interface.%s.", interface), cleanup, ERROR, "amxc_string_setf failed");

    rv = amxb_call(bus, amxc_string_get(&objectname, 0), function, args, ret, 5);
    when_false_trace(rv == AMXB_STATUS_OK, exit, ERROR, "amxb_call(%s, ...) failed: %d", function, rv);

cleanup:
    amxc_string_clean(&objectname);

exit:
    return rv == AMXB_STATUS_OK;
}

static AMXM_CONSTRUCTOR module_init(void) {
    struct {
        const char* name;
        amxm_callback_t cb;
    } function[] = {
        {"get_services", get_services},
        {"set_service", set_service},
        {"delete_service", delete_service},
        {"get_policies", get_policies},
        {"set_policy", set_policy},
        {"delete_policy", delete_policy},
        {"get_interfaces", get_interfaces},
        {"set_interface", set_interface},
        {"delete_interface", delete_interface},
        {"get_interface_whitelists", get_interface_whitelists},
        {"set_interface_whitelist_network", set_interface_whitelist_network},
        {"delete_interface_whitelist_network", delete_interface_whitelist_network},
        {"set_pinhole", set_pinhole},
        {"delete_pinhole", delete_pinhole},
        {"set_chain_rule", set_chain_rule},
        {"get_chain_rules", get_chain_rules},
        {"delete_chain_rule", delete_chain_rule},
    };
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;
    size_t i = 0;

    if(0 != amxm_module_register(&mod, so, "fw")) {
        SAH_TRACEZ_ERROR(ME, "Failed to register module");
        goto exit;
    }

    for(i = 0; i < sizeof(function) / sizeof(function[0]); i++) {
        if(0 != amxm_module_add_function(mod, function[i].name, function[i].cb)) {
            SAH_TRACEZ_WARNING(ME, "Failed to register function %s", function[i].name);
        }
    }

exit:
    return 0;
}
