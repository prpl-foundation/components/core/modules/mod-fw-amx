/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __MOD_FW_AMX_H__
#define __MOD_FW_AMX_H__

#include <stdint.h>
#include <amxc/amxc.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define ME "mod_fw_amx"

#define NO_OUTNAME NULL

typedef struct _translate {
    const char* in_name;
    const char* out_name;
    uint32_t type;
    int (* action)(const amxc_var_t* data, amxc_var_t* arg);
} translate_t;

int set_service(const char* function_name,
                amxc_var_t* args,
                amxc_var_t* ret);

int delete_service(const char* function_name,
                   amxc_var_t* args,
                   amxc_var_t* ret);

int get_services(const char* function_name,
                 amxc_var_t* args,
                 amxc_var_t* ret);

int set_policy(const char* function_name,
               amxc_var_t* args,
               amxc_var_t* ret);

int delete_policy(const char* function_name,
                  amxc_var_t* args,
                  amxc_var_t* ret);

int get_policies(const char* function_name,
                 amxc_var_t* args,
                 amxc_var_t* ret);

int set_interface(const char* function_name,
                  amxc_var_t* args,
                  amxc_var_t* ret);

int delete_interface(const char* function_name,
                     amxc_var_t* args,
                     amxc_var_t* ret);

int get_interfaces(const char* function_name,
                   amxc_var_t* args,
                   amxc_var_t* ret);

int get_interface_whitelists(const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret);

int delete_interface_whitelist_network(const char* function_name,
                                       amxc_var_t* args,
                                       amxc_var_t* ret);

int set_interface_whitelist_network(const char* function_name,
                                    amxc_var_t* args,
                                    amxc_var_t* ret);

int set_pinhole(const char* function_name,
                amxc_var_t* args,
                amxc_var_t* ret);

int delete_pinhole(const char* function_name,
                   amxc_var_t* args,
                   amxc_var_t* ret);

int set_chain_rule(const char* function_name,
                   amxc_var_t* args,
                   amxc_var_t* ret);

int get_chain_rules(const char* function_name,
                    amxc_var_t* args,
                    amxc_var_t* ret);

int delete_chain_rule(const char* function_name,
                      amxc_var_t* args,
                      amxc_var_t* ret);

bool firewall_call(const char* function, amxc_var_t* args, amxc_var_t* ret);
bool firewall_interface_call(const char* interface, const char* function, amxc_var_t* args, amxc_var_t* ret);

int translate_iana_protocols_name(const amxc_var_t* data, amxc_var_t* fw_arg);

#ifdef __cplusplus
}
#endif

#endif // __MOD_FW_AMX_H__
