mod-fw-amx
==========

This module should be used by plugins to interact with the firewall plugin. These functions will create/modify/delete firewall data model instances.

Best practices
--------------
- Use an `id` that describes where the instance comes from. This makes it easier to debug the firewall and avoids conflicting names. E.g.: 'http' for a service with ports 80/443.

Description of API
------------------
```
[in]        : the amxc_var_t is used as input
[out]       : the amxc_var_t is used as output
(mandatory) : this key-value pair is mandatory
#           : short description of the function, the details can be found in the firewall plugin's documentation
```

Services
--------
For 'Firewall.Service.' instances. Read the firewall plugin's documentation for more information about this object.

```
# Plugins should call 'set_service' to create/modify a service. Key-value pair `id` is optional. There are 3 cases:
# 1. If `id` is not in args then the firewall plugin will generate one and create a new instance.
# 2. If `id` is in args but there is no instance that matches "Alias==id" then the firewall plugin will create a new instance with Alias=id.
# 3. If `id` is in args and there is an instance that matches "Alias==id" then the firewall plugin will modify this instance.
# In any case will the `id` be returned in ret. Depending on how you select `id` you should remember it to call delete_service.
- set_service()
  - [in] args as list of htables
    - htable item
      - id
      - destination_port
      - enable
      - icmp_type
      - interface
      - ipversion
      - protocol (mandatory)
      - source_prefix
  - [out] ret as htable
    - htable item
      - id

# Plugins should call 'delete_service' to remove a service with "Alias==id".
- delete_service()
  - [in] args as list of htables
    - htable item
      - id (mandatory)
  - [out] ret
```

Pinholes
--------
For 'Firewall.Pinhole.' instances. Read the firewall plugin's documentation for more information about this object.

```
# Plugins should call 'set_pinhole' to create/modify a pinhole. Key-value pair `id` is optional. There are 3 cases:
# 1. If `id` is not in args then the firewall plugin will generate one and create a new instance.
# 2. If `id` is in args but there is no instance that matches "Alias==id" then the firewall plugin will create a new instance with Alias=id.
# 3. If `id` is in args and there is an instance that matches "Alias==id" then the firewall plugin will modify this instance.
# In any case will the `id` be returned in ret. Depending on how you select `id` you should remember it to call delete_pinhole.
- set_pinhole()
  - [in] args as list of htables
    - htable item
      - id
      - origin
      - interface
      - internal_client
      - source_prefix
      - description
      - source_port
      - destination_port
      - protocol
      - enable
  - [out] ret as htable
    - htable item
      - id

# Plugins should call 'delete_pinhole' to remove a pinhole with "Alias==id".
- delete_pinhole()
  - [in] args as list of htables
    - htable item
      - id (mandatory)
  - [out] ret
```

Policies
--------
For 'Firewall.Policy.' instances. Read the firewall plugin's documentation for more information about this object.

```
# Plugins should call 'set_policy' to create/modify a policy. Key-value pair `id` is optional. There are 3 cases:
# 1. If `id` is not in args then the firewall plugin will generate one and create a new instance.
# 2. If `id` is in args but there is no instance that matches "Alias==id" then the firewall plugin will create a new instance with Alias=id.
# 3. If `id` is in args and there is an instance that matches "Alias==id" then the firewall plugin will modify this instance.
# Depending on how you select `id` you should remember it to call delete_policy.
- set_policy()
  - [in] args as list of htables
    - htable item
      - id
      - source_interface
      - destination_interface
      - ipversion
      - enable
  - [out] ret

# Plugins should call 'delete_policy' to remove a policy with "Alias==id".
- delete_policy()
  - [in] args as list of htables
    - htable item
      - id (mandatory)
  - [out] ret
```
