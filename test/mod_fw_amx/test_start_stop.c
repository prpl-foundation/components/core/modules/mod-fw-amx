/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <amxut/amxut_dm.h>

#include "test_start_stop.h"
#include "mock.h"

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;
static amxm_shared_object_t* so = NULL;

int test_setup(void** state) {
    amxut_bus_setup(state);
    parser = amxut_bus_parser();
    dm = amxut_bus_dm();
    test_init_dummy_dm(dm, parser);

    assert_int_equal(amxm_so_open(&so, "mod", MOD_DIR "/mod-fw-amx.so"), 0);

    return 0;
}

int test_teardown(void** state) {
    amxm_close_all();

    test_clean_dummy_dm();

    amxm_so_close(&so);

    amxut_bus_teardown(state);

    return 0;
}

void test_set_service(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t* last_args = NULL;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "interface", "Device.IP.Interface.2.");
    amxc_var_add_key(uint32_t, &args, "destination_port", 53);
    amxc_var_add_key(cstring_t, &args, "protocol", "UDP,TCP");
    amxc_var_add_key(uint32_t, &args, "ipversion", 4);
    amxc_var_add_key(bool, &args, "enable", true);
    amxc_var_add_key(uint32_t, &args, "icmp_type", 8);
    amxc_var_add_key(cstring_t, &args, "source_prefix", "192.168.1.0/24");

    rv = amxm_execute_function("mod", "fw", "set_service", &args, &ret);
    assert_int_equal(rv, 0);

    print_message("assert that Firewall.setService was called with the right arguments\n");
    last_args = get_last_set_service_args();
    assert_non_null(GET_ARG(last_args, "interface"));
    assert_non_null(GET_ARG(last_args, "destinationPort")); // should be translated from 'destination_port'
    assert_non_null(GET_ARG(last_args, "protocol"));
    assert_non_null(GET_ARG(last_args, "ipversion"));
    assert_non_null(GET_ARG(last_args, "enable"));
    assert_non_null(GET_ARG(last_args, "icmpType"));
    assert_non_null(GET_ARG(last_args, "sourcePrefix"));
    print_message("assert that Firewall.setService returns some id\n");
    assert_non_null(GET_ARG(&ret, "id"));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_delete_service(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    const char* id = "foo";
    amxc_var_t* last_args = NULL;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", id);

    rv = amxm_execute_function("mod", "fw", "delete_service", &args, &ret);
    assert_int_equal(rv, 0);

    print_message("assert that Firewall.deleteService was called with the right arguments\n");
    last_args = get_last_delete_service_args();
    assert_non_null(GET_ARG(last_args, "alias")); // should be translated from 'id'

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_pinhole(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t* last_args = NULL;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "origin", "pcp");
    amxc_var_add_key(cstring_t, &args, "interface", "Device.IP.Interface.2.");
    amxc_var_add_key(cstring_t, &args, "internal_client", "192.168.1.2");
    amxc_var_add_key(cstring_t, &args, "source_prefix", "192.168.1.0/24");
    amxc_var_add_key(cstring_t, &args, "description", "foo bar baz");
    amxc_var_add_key(uint32_t, &args, "source_port", 50000);
    amxc_var_add_key(uint32_t, &args, "destination_port", 50001);
    amxc_var_add_key(cstring_t, &args, "protocol", "TCP");
    amxc_var_add_key(cstring_t, &args, "lease_duration", "300");
    amxc_var_add_key(bool, &args, "enable", true);

    rv = amxm_execute_function("mod", "fw", "set_pinhole", &args, &ret);
    assert_int_equal(rv, 0);

    print_message("assert that Firewall.setPinhole was called with the right arguments\n");
    last_args = get_last_set_service_args();
    assert_non_null(GET_ARG(last_args, "origin"));
    assert_non_null(GET_ARG(last_args, "interface"));
    assert_non_null(GET_ARG(last_args, "internalClient"));
    assert_non_null(GET_ARG(last_args, "sourcePrefix"));
    assert_non_null(GET_ARG(last_args, "description"));
    assert_non_null(GET_ARG(last_args, "sourcePort"));
    assert_non_null(GET_ARG(last_args, "destinationPort"));
    assert_non_null(GET_ARG(last_args, "protocol"));
    assert_non_null(GET_ARG(last_args, "enable"));
    assert_non_null(GET_ARG(last_args, "leaseDuration"));
    print_message("assert that Firewall.setPinhole returns some id\n");
    assert_non_null(GET_ARG(&ret, "id"));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_delete_pinhole(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    const char* id = "foo";
    amxc_var_t* last_args = NULL;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", id);

    rv = amxm_execute_function("mod", "fw", "delete_pinhole", &args, &ret);
    assert_int_equal(rv, 0);

    print_message("assert that Firewall.deletePinhole was called with the right arguments\n");
    last_args = get_last_delete_service_args();
    assert_non_null(GET_ARG(last_args, "alias")); // should be translated from 'id'

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_iana_translation(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t* last_args = NULL;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "interface", "Device.IP.Interface.2.");
    amxc_var_add_key(uint32_t, &args, "destination_port", 53);
    amxc_var_add_key(cstring_t, &args, "protocol", "UDP,TCP");
    amxc_var_add_key(uint32_t, &args, "ipversion", 4);
    amxc_var_add_key(bool, &args, "enable", true);
    amxc_var_add_key(uint32_t, &args, "icmp_type", 8);
    amxc_var_add_key(cstring_t, &args, "source_prefix", "192.168.1.0/24");

    rv = amxm_execute_function("mod", "fw", "set_service", &args, &ret);
    assert_int_equal(rv, 0);

    last_args = get_last_set_service_args();
    assert_non_null(GET_ARG(last_args, "protocol"));
    assert_string_equal(GET_CHAR(last_args, "protocol"), "17,6");

    amxc_var_set(cstring_t, amxc_var_get_key(&args, "protocol", AMXC_VAR_FLAG_DEFAULT), "17,TCP");

    rv = amxm_execute_function("mod", "fw", "set_service", &args, &ret);
    assert_int_equal(rv, 0);

    last_args = get_last_set_service_args();
    assert_non_null(GET_ARG(last_args, "protocol"));
    assert_string_equal(GET_CHAR(last_args, "protocol"), "17,6");

    amxc_var_set(cstring_t, amxc_var_get_key(&args, "protocol", AMXC_VAR_FLAG_DEFAULT), "17,6,FAKEPROTO");

    rv = amxm_execute_function("mod", "fw", "set_service", &args, &ret);
    assert_int_equal(rv, 0);

    last_args = get_last_set_service_args();
    assert_non_null(GET_ARG(last_args, "protocol"));
    assert_string_equal(GET_CHAR(last_args, "protocol"), "17,6,FAKEPROTO");

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_policy(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t* last_args = NULL;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", "randomid");
    amxc_var_add_key(cstring_t, &args, "source_interface", "siface");
    amxc_var_add_key(cstring_t, &args, "destination_interface", "diface");
    amxc_var_add_key(cstring_t, &args, "level", "Medium");
    amxc_var_add_key(uint32_t, &args, "ipversion", 4);
    amxc_var_add_key(bool, &args, "enable", true);
    amxc_var_add_key(bool, &args, "persistent", true);

    rv = amxm_execute_function("mod", "fw", "set_policy", &args, &ret);
    assert_int_equal(rv, 0);

    print_message("assert that Firewall.set_policy was called with the right arguments\n");
    last_args = get_last_set_service_args();
    assert_non_null(GET_ARG(last_args, "sourceInterface"));
    assert_non_null(GET_ARG(last_args, "destinationInterface")); // should be translated from 'destination_port'
    assert_non_null(GET_ARG(last_args, "level"));
    assert_non_null(GET_ARG(last_args, "ipversion"));
    assert_non_null(GET_ARG(last_args, "enable"));
    assert_non_null(GET_ARG(last_args, "persistent"));
    print_message("assert that Firewall.set_policy does not return an id\n");
    assert_null(GET_ARG(&ret, "id"));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_delete_policy(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    const char* id = "foo";
    amxc_var_t* last_args = NULL;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", id);

    rv = amxm_execute_function("mod", "fw", "delete_policy", &args, &ret);
    assert_int_equal(rv, 0);

    print_message("assert that Firewall.delete_policy was called with the right arguments\n");
    last_args = get_last_delete_service_args();
    assert_non_null(GET_ARG(last_args, "alias")); // should be translated from 'id'

    print_message("assert that Firewall.delete_policy does not return an id\n");
    assert_null(GET_ARG(&ret, "id"));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_interface(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", "randomid");
    amxc_var_add_key(bool, &args, "NATEnable", true);
    amxc_var_add_key(bool, &args, "StrictNATInterface", true);
    amxc_var_add_key(cstring_t, &args, "IsolateInterface", "iface");
    amxc_var_add_key(bool, &args, "SourceAddress6Spoofing", true);
    amxc_var_add_key(bool, &args, "SourceAddress4Spoofing", true);
    amxc_var_add_key(bool, &args, "IsNATInterface", true);
    amxc_var_add_key(bool, &args, "persistent", true);

    rv = amxm_execute_function("mod", "fw", "set_interface", &args, &ret);
    assert_int_equal(rv, -1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_delete_interface(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    const char* id = "foo";
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", id);

    rv = amxm_execute_function("mod", "fw", "delete_interface", &args, &ret);
    assert_int_equal(rv, -1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_chain_rule(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", "randomid");
    amxc_var_add_key(cstring_t, &args, "chain", "Custom");
    amxc_var_add_key(cstring_t, &args, "target", "Accept");
    amxc_var_add_key(uint32_t, &args, "destination_port", 50000);
    amxc_var_add_key(uint32_t, &args, "source_port", 51820);
    amxc_var_add_key(cstring_t, &args, "destination_prefix", "172.16.10.0/24");
    amxc_var_add_key(cstring_t, &args, "source_prefix", "192.168.1.0/24, 10.0.1.0/16");
    amxc_var_add_key(cstring_t, &args, "protocol", "");
    amxc_var_add_key(uint32_t, &args, "ipversion", 4);
    amxc_var_add_key(bool, &args, "enable", true);
    amxc_var_add_key(cstring_t, &args, "description", "test_fw_rule");
    amxc_var_add_key(cstring_t, &args, "destination_mac", "");
    amxc_var_add_key(cstring_t, &args, "source_mac", "");
    amxc_var_add_key(bool, &args, "persistent", true);

    rv = amxm_execute_function("mod", "fw", "set_chain_rule", &args, &ret);
    assert_int_equal(rv, -1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_get_chain_rule(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    const char* id = "foo";
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", id);
    amxc_var_add_key(cstring_t, &args, "chain", "Custom");

    rv = amxm_execute_function("mod", "fw", "delete_chain_rule", &args, &ret);
    assert_int_equal(rv, -1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_delete_chain_rule(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    const char* id = "randomid";
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", id);
    amxc_var_add_key(cstring_t, &args, "chain", "Custom");

    rv = amxm_execute_function("mod", "fw", "delete_chain_rule", &args, &ret);
    assert_int_equal(rv, -1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
