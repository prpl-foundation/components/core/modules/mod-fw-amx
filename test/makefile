PKG_CONFIG_LIBDIR := /usr/lib/pkgconfig:/lib/pkgconfig:/usr/lib/x86_64-linux-gnu/pkgconfig:$(PKG_CONFIG_LIBDIR)
MACHINE = $(shell $(CC) -dumpmachine)
SUBDIRS := $(wildcard mod_fw_*)
OBJDIR = ../output/$(MACHINE)/coverage
COVERREPORT = report

CFLAGS += -fprofile-arcs -ftest-coverage
LDFLAGS += -fprofile-arcs -ftest-coverage

export OBJDIR

run: $(OBJDIR)/ | mod-fw-amx
	@for dir in $(SUBDIRS); do make -C $$dir $@  || exit -1; done
	@rm -rf $(OBJDIR)/test* $(OBJDIR)/dummy* $(OBJDIR)/common* $(OBJDIR)/mock*

clean:
	rm -rf $(OBJDIR)
	rm -rf $(OBJDIR)/$(COVERREPORT)
	find .. -name "run_test" -delete

coverage: $(OBJDIR)/$(COVERREPORT)/
	@cd $(OBJDIR) && ln -s ../../../src/mod_fw_amx.c mod_fw_amx.c
	@cd $(OBJDIR) && ln -s ../../../src/pinholes.c pinholes.c
	@cd $(OBJDIR) && ln -s ../../../src/services.c services.c
	@cd $(OBJDIR) && ln -s ../../../src/interfaces.c interfaces.c
	@cd $(OBJDIR) && ln -s ../../../src/utils.c utils.c
	@cd $(OBJDIR) && ln -s ../../../src/policies.c policies.c
	@cd $(OBJDIR) && ln -s ../../../src/chains.c chains.c
	@cd $(OBJDIR) && \
	for i in $$(find . -type f -iname "*.o"); do \
		gcov -c -b -f --long-file-names --preserve-paths $$i > /dev/null; \
	done
	@cd $(OBJDIR) && for i in $$(find . -name "*.h.gcov"); do rm $$i > /dev/null; done
	cd $(OBJDIR) && gcovr -k -p -r ../../.. -s -g --html --html-details -o ./$(COVERREPORT)/index.html
	cd $(OBJDIR) && gcovr -k -p -r ../../.. -s -g | tee ./$(COVERREPORT)/gcovr_summary.txt

$(OBJDIR)/:
	mkdir -p $@

$(OBJDIR)/$(COVERREPORT)/:
	mkdir -p $@


mod-fw-amx:
	make -C ../src all UNIT_TEST=y

.PHONY: run clean coverage
