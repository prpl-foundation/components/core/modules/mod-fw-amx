/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include "mock.h"

static const char* const odl_mock_fw = "../common/mock_firewall.odl";

static amxc_var_t last_set_service_args;
static amxc_var_t last_delete_service_args;

static amxd_status_t _setService(UNUSED amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    amxc_var_copy(&last_set_service_args, args);

    // mod-fw-amx will convert 'alias' to 'id'
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    if(GET_ARG(args, "alias") != NULL) {
        amxc_var_t* var = amxc_var_add_new_key(ret, "Alias");
        amxc_var_copy(var, GET_ARG(args, "alias"));
    } else {
        amxc_var_add_key(cstring_t, ret, "Alias", "cpe-service-1");
    }

    return amxd_status_ok;
}

static amxd_status_t _deleteService(UNUSED amxd_object_t* object,
                                    UNUSED amxd_function_t* func,
                                    UNUSED amxc_var_t* args,
                                    UNUSED amxc_var_t* ret) {
    amxc_var_copy(&last_delete_service_args, args);

    return amxd_status_ok;
}

static void test_init_dummy_fn_resolvers(amxo_parser_t* parser) {
    amxo_resolver_ftab_add(parser, "setService", AMXO_FUNC(_setService));
    amxo_resolver_ftab_add(parser, "deleteService", AMXO_FUNC(_deleteService));
    amxo_resolver_ftab_add(parser, "setPinhole", AMXO_FUNC(_setService));
    amxo_resolver_ftab_add(parser, "deletePinhole", AMXO_FUNC(_deleteService));
    amxo_resolver_ftab_add(parser, "setPolicy", AMXO_FUNC(_setService));
    amxo_resolver_ftab_add(parser, "deletePolicy", AMXO_FUNC(_deleteService));
    amxo_resolver_ftab_add(parser, "setInterface", AMXO_FUNC(_setService));
    amxo_resolver_ftab_add(parser, "deleteInterface", AMXO_FUNC(_deleteService));
    amxo_resolver_ftab_add(parser, "setCustomRule", AMXO_FUNC(_setService));
    amxo_resolver_ftab_add(parser, "deleteCustomRule", AMXO_FUNC(_deleteService));
}

void test_init_dummy_dm(amxd_dm_t* dm, amxo_parser_t* parser) {
    amxd_object_t* root_obj = NULL;

    amxc_var_init(&last_set_service_args);
    amxc_var_init(&last_delete_service_args);

    test_init_dummy_fn_resolvers(parser);

    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(parser, odl_mock_fw, root_obj), 0);
}

void test_clean_dummy_dm(void) {
    amxc_var_clean(&last_set_service_args);
    amxc_var_clean(&last_delete_service_args);
}

amxc_var_t* get_last_set_service_args(void) {
    return &last_set_service_args;
}

amxc_var_t* get_last_delete_service_args(void) {
    return &last_delete_service_args;
}
